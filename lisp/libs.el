(require 'package)
(add-to-list 'package-archives 
             '("melpa" . "http://melpa.milkbox.net/packages/") t)

(package-initialize)

(defvar favorite
  '(
    smart-compile
    flycheck
    matlab-mode
    haskell-mode
    key-chord
    moe-theme
    ))
(dolist (package favorite)
  (unless (package-installed-p package)
    (package-install package)))

(add-to-list 'custom-theme-load-path
             "~/.emacs.d/elpa/moe-theme-20150307.1907")
(load-theme 'moe-dark t)
; (setq matlab-indent-function t)
; (setq matlab-shell-command "/Applications/MATLAB_R2014a.app/bin/matlab")

; Haskell
(add-to-list 'auto-mode-alist '("\\.hs$" . haskell-mode))
(add-to-list 'auto-mode-alist '("\\.lhs$" . literate-haskell-mode))
(add-to-list 'auto-mode-alist '("\\.cabal\\'" . haskell-cabal-mode))

; flycheck
(add-hook 'after-init-hook #'global-flycheck-mode)

; smart-compile
(require 'smart-compile)
(global-set-key (kbd "C-c c") 'smart-compile)
(setq smart-compile-alist
      (append
       '(("\\.tex$" . "lualatex %f"))
              smart-compile-alist))

;; view-modeを有効・無効にする
;; 0.04秒以内に qw を同時押しでview-modeに移行
(require 'key-chord)
(setq key-chord-two-keys-delay 0.04)
(key-chord-mode t)
(key-chord-define-global "qw" 'view-mode)


