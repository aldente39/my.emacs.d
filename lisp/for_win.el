(setq default-input-method "W32-IME")
(setq-default w32-ime-mode-line-state-indicator "[--]")
(setq w32-ime-mode-line-state-indicator-list '("[--]" "[あ]" "[--]"))
(w32-ime-initialize)
;; 日本語入力時にカーソルの色を変える設定 (色は適宜変えてください)
(add-hook 'w32-ime-on-hook '(lambda () (set-cursor-color "coral4")))
(add-hook 'w32-ime-off-hook '(lambda () (set-cursor-color "black")))

(defun powershell-command ()
  (interactive)
  (let (cmd)
    (setq cmd (read-string "PowerShell command: "))
    (shell-command (concat "powershell -Command " cmd))))
(global-set-key (kbd "C-c s") 'powershell-command)

(set-face-attribute 'default nil :family "Consolas" :height 104)
(set-fontset-font nil 'japanese-jisx0208 (font-spec :family "MS Gothic"))
(setq face-font-rescale-alist '(("MS Gothic" . 1.08)))

