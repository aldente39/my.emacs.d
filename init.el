;;; クリップボードを共有
(setq x-select-enable-clipboard t)

; ニホンゴ
(set-language-environment "Japanese")
(prefer-coding-system 'utf-8)

;; 165が\, 92が\（バックスラッシュ）を表す
(define-key global-map [165] [92])

; tabでスペース4つ，インデント幅は4
(setq-default c-basic-offset 4
              tab-width 4
              indent-tabs-mode nil)

;; ;設定ファイルの再読み込みをF12で行う
;; (global-set-key
;;  [f12] 'eval-buffer)

; コメントアウト
(setq-default transient-mark-mode t)
(setq comment-style 'multi-line)

; C-c c で compile コマンドを呼び出す
(define-key mode-specific-map "c" 'compile)

; C-c s で外部コマンド入力待ちに
(define-key mode-specific-map "s" 'shell-command)

; ウィンドウ間の移動
(global-set-key (kbd "C-c b")  'windmove-left)
(global-set-key (kbd "C-c n")  'windmove-down)
(global-set-key (kbd "C-c p")    'windmove-up)
(global-set-key (kbd "C-c f") 'windmove-right)

; 新規フレーム
(define-key mode-specific-map "n" 'make-frame-command)
; 新規フレームでファイルを開く
(global-set-key (kbd "C-c C-f") 'find-file-other-frame)
; フレームを閉じる
(global-set-key (kbd "C-c C-c") (kbd "C-x 5 0"))

; 一行削除を行うマクロ
(setq windmove-wrap-around t)
(fset 'delete-line
    [?\C-a ?\C-@ ?\C-n backspace])
; C-, で一行削除
(global-set-key (kbd "C-,") 'delete-line)

; C-c i でインデントの整形
(global-set-key (kbd "C-c i") 'indent-region)

; C-:でM-x
(global-set-key (kbd "C-:") 'execute-extended-command)

; M-x qでも終了
(defalias 'q 'save-buffers-kill-emacs)


(global-set-key (kbd "M-RET") 'execute-extended-command)

;; マウスホイールでスクロール
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1))) ;; one line at a time
(setq mouse-wheel-progressive-speed nil) ;; don't accelerate scrolling
(setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse

; compilation modeでの自動スクロール
(setq compilation-scroll-output t)


;;; ミニバッファの履歴を保存する
(savehist-mode 1)

; ツールチップは無効
(tooltip-mode nil)
(setq tooltip-use-echo-area t)

; サーバとして立ち上げる
(server-start)

; カタカナひらがなキーでHyper
;(w32-register-hot-key [<hiragana-katakana>])
;(global-set-key [(hiragana-katakana)] 'hyper)

; バックアップの設定-------------------------------

; バックアップファイルを作らない
(setq backup-inhibited t)

; オートセーブもいらない
(setq auto-save-default nil)

; 終了時にオートセーブファイルを消す
(setq delete-auto-save-files t)


; 見た目の設定------------------------------------

; 行番号の表示
(global-linum-mode t)

; 対応する括弧をハイライト
(show-paren-mode)

; 行のハイライト
(global-hl-line-mode)

; ツールバー非表示
(tool-bar-mode 0)

;; 英語フォント
;(set-face-attribute 'default nil
;             :family "Menlo" ;; font
;             :height 120)    ;; font size

;; 日本語フォント
;(set-fontset-font
; nil 'japanese-jisx0208
; (font-spec :family "MS Gothic")) ;; font

;(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
; '(custom-safe-themes
;   (quote
;    ("fc5fcb6f1f1c1bc01305694c59a1a861b008c534cae8d0e48e4d5e81ad718bc6" default)))
; '(initial-frame-alist (quote ((top . 0) (left . 100) (width . 80) (height . 40)))))
;(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
; )

; カラーテーマ
;(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/")
;(load-theme 'solarized-light t)

; カーソルの形を変更
(add-to-list 'default-frame-alist '(cursor-type . bar))

; IME OFF時の初期カーソルカラー
(set-cursor-color "orange")

; IME ON/OFF時のカーソルカラー
;(add-hook 'input-method-activate-hook
;          (lambda() (set-cursor-color "green")))
;(add-hook 'input-method-inactivate-hook
;          (lambda() (set-cursor-color "red")))



(setq load-path
      (append '(
                "~/.emacs.d/lisp/"
                ) load-path))

(load "libs")

(if (eq system-type 'windows-nt) (load "for_win"))
(if (eq system-type 'darwin) (load "for_mac"))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("cf3e206f8fe53354921a492fb58eee2efffef3251032387752f725c789f612ff" default))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
